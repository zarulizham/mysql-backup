<?php 

$project_path = '/Applications/MAMP/htdocs/database-backup/';
$mysqldump_path = '/Applications/MAMP/Library/bin/mysqldump';
$minimum_storage_alert = 200; // MB
$sleep_time = 20; // second

$dropboxData = array(
    array(
        "email"         => "backup1@example.com",
        "app_key"       => "umexample0i2pxx",
        "app_secret"    => "fofx4gexamplebv",
        "token"         => "tkLj7TE3IKAAAAAAAAAABXK4Lx8vDTaJromB1bJl-exampleM6T7HR016nIzhP0"
    ),
    array(
        "email"         => "backup2@example.com",
        "app_key"       => "19zexamplepta3r",
        "app_secret"    => "4exampleyafdmc2",
        "token"         => "2hCCKe1XaYAAAAAAAAAABajxucfS94j8CGZ-tiFexampleniw258pgOR1b4MY"
    ),
);

// Set which account to be used
$data = $dropboxData[0];

$databases = array(
    array(
        'name'              => 'database_name_1',
        'username'          => 'root',
        'password'          => 'root',
        'host'              => 'localhost'
    ),
    array(
        'name'              => 'database_name_2',
        'username'          => 'root',
        'password'          => 'root',
        'host'              => '103.31.103.44'
    )
);

function sendEmail($subject, $message) {

    $mailgun_api_key = 'api:key-sample4c4059examplea66af7ffd5key';
    $mailgun_domain = 'mailing.example.com';
    $email_from = 'support@example.com';
    $email_to = 'receiver@example.com';

    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $mailgun_api_key);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    
    $plain = strip_tags($message);
    
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v2/'.$mailgun_domain.'/messages');
    curl_setopt($ch, CURLOPT_POSTFIELDS, array(
        'from'      => $email_from,
        'to'        => $to,
        'subject'   => $subject,
        'html'      => $message,
        'text'      => $plain
    ));
    
    $j = json_decode(curl_exec($ch));
    
    $info = curl_getinfo($ch);
    
    if ($info['http_code'] != 200) {
        error("Mailgun Error");
    }
    
    curl_close($ch);
    
    return $j;
}
