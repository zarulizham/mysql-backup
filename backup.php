<?php
	set_time_limit(120);
	error_reporting(E_ALL);
    ini_set('display_errors', 1);
    date_default_timezone_set("Asia/Kuala_Lumpur");

    include 'vendor/autoload.php';
    include 'common.php';
	
	use Kunnu\Dropbox\DropboxFile;
	use Kunnu\Dropbox\DropboxApp;
	use Kunnu\Dropbox\Dropbox;
	
	$app = new DropboxApp($data['app_key'], $data['app_secret'], $data['token']);
	$dropbox = new Dropbox($app);
	
	$accountSpace = $dropbox->getSpaceUsage();

	// Space Used
	$used = $accountSpace['used'];
	
	// Space Allocated
    $allocated = $accountSpace['allocation']['allocated'];
    
    // Free Space
    $free = ($allocated - $used)/1000000;

	echo "Used: ".number_format($used/1000000)." MB <br>";
	echo "Allocated: ".number_format($allocated/1000000)." MB <br>";
	echo "Free: ".number_format(($allocated-$used)/1000000)." MB <br>";
    echo "Free ".number_format($free)." MB <br><br>";

	if ($free < $minimum_storage_alert) {
		$message = $data['email'].' has low free space.<br>';
		$message .= "Used: ".number_format($used/1000000)." MB <br>";
		$message .= "Allocated: ".number_format($allocated/1000000)." MB <br>";
		$message .= "Free: ".number_format(($allocated-$used)/1000000)." MB <br>";
		sendEmail("Dropbox Storage Low", $message);
    }
	
	$folderMonth = date('F');
	$folderDay = date('Ymd_H');
	
	$folderName = $folderMonth.'/'.$folderDay;
	$folder = $project_path.$folderName;
	
	if (!is_dir($project_path.$folderMonth)) {
		mkdir($project_path.$folderMonth, 0777, true);
	}
	
	if (is_dir($project_path.$folderMonth.'/'.$folderDay)) {
	} else {
		mkdir($project_path.$folderMonth.'/'.$folderDay, 0777, true);
    }
    
	$filename = date('Ymd_His');

    foreach ($databases as $index => $db) {
        $path = $project_path.$folderName.'/'.$filename.'_'.$db['name'].'.sql';
        $databases[$index]['path'] = $path;
        exec($mysqldump_path.' --routines --user='.$db['username'].' --password="'.$db['password'].'" --host='.$db['host'].' '.$db['name'].' > "'.$path.'"');
    }

    sleep($sleep_time);
    
    // Upload to Dropbox
    foreach ($databases as $index => $db) {
        $file = $dropbox->simpleUpload($db['path'], "/".$folderMonth."/".$db['name']."/".$filename."_".$db['name'].".sql", ['autorename' => true]);
        echo $file->getName().'<br>';
        unlink($db['path']);
    }