# Getting Started

This repository will help to automate MySQL backup by using cronjob.

To start working with the Dropbox API, you'll need an App. You can create a new app for the Dropbox API here: https://www.dropbox.com/developers/apps

# Installation

1. Clone this repository. `git clone https://gitlab.com/zarulizham/mysql-backup`
2. Enter project directory. `cd mysql-backup`
3. Install dependency by run `composer install`
4. Copy common.php file. `cp example.common.php common.php`
5. Edit details in `common.php`
6. Run `backup.php` in your browser
   
> To automatate this script, add backup.php to cronjob

# Security Issue?
Report to zarul.izham@gmail.com

# Special Thanks to
1. kunalvarma05/dropbox-php-sdk - https://github.com/kunalvarma05/dropbox-php-sdk